import React from 'react';
import ReactDOM from 'react-dom';
import Engage from './Engage';
import './main.scss';

const root = document.getElementById('root');
ReactDOM.render(<Engage/>, root);